import sys
import os


db_names = ['tpcc10', 'tpcc20', 'tpcc40', 'tpcc64']
pool_size = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]
disk_quota = [1, 2, 4, 8, 16, 32, 48]
cfs_period = [100000, 250000, 500000, 1000000]
# runtime in seconds
runtime = 190
# warmup time in seconds
warmup = 15
# connections
connections = 10

nfs_server_ip = '192.168.122.25'
sql_server_ip ='192.168.122.199'
options = f'-h {sql_server_ip} -u root -paries123 '
create_cmd = 'mysqladmin create ' + options
drop_cmd = 'mysqladmin drop --force ' + options
create_tbl='/root/datacollection/tpcc-mysql/create_table.sql'
add_fkey_idx='/root/datacollection/tpcc-mysql/add_fkey_idx.sql'
tpcc_load = '/root/datacollection/tpcc-mysql/tpcc_load'
tpcc_run = '/root/datacollection/tpcc-mysql/tpcc_start'
report_base = '/root/datacollection/reports'
if os.path.isdir(report_base) is False:
    os.mkdir(report_base)
mysqld_dir = '/sys/fs/cgroup/cpu/mysqld'


def mount_nfs_part():
    cmd = f'ssh {sql_server_ip} \"cat /proc/mounts |grep nfsshare\"'
    op = os.popen(cmd).read().strip('\n')
    if op == '' :
        cmd = f'ssh {sql_server_ip} \"mount {nfs_server_ip}:/opt/nfsshare /nfsdb\"'
        os.system(cmd)
    else:
        print("NFS partition mounted already...")


def setup_nfs_server(disk):
    cmd = f'ssh {nfs_server_ip} \"cat /proc/mounts |grep {disk}\"'
    op = os.popen(cmd).read().strip('\n')
    if op == '':
        cmd = f'ssh {nfs_server_ip} \"mount /dev/{disk} /opt/nfsshare\"'
    else:
        print(f"Disk {disk} already mounted")

    cmd = f'ssh {nfs_server_ip} \"service nfs-server stop\"'
    os.system(cmd)

    cmd = f'ssh {nfs_server_ip} \"echo /opt/nfsshare *\(rw,async,insecure,no_root_squash\) > /etc/exports\"'
    os.system(cmd)
    cmd = f'ssh {nfs_server_ip} \"exportfs -ra; service nfs-server start\"'
    os.system(cmd)


def setup_mysql_server(pool_sz):
    filename = '/etc/my.cnf.d/mariadb-server.cnf'
    proc_pid = f'{mysqld_dir}/cgroup.procs'
    size = pool_sz * 1048576
    # shell cmd: sed -i 's/innodb_buffer_pool_size=.*/innodb_buffer_pool_size=4194304/g' /etc/my.cnf.d/mariadb-server.cnf
    cmd = f'ssh {sql_server_ip} \"sed -i \'s/innodb_buffer_pool_size=.*/innodb_buffer_pool_size={size}/g\' {filename}\"'
    os.system(cmd)
    cmd = f'ssh {sql_server_ip} \"service mariadb start\"'
    os.system(cmd)

    cmd = f'ssh {sql_server_ip} \"if [ ! -d {mysqld_dir} ]; then mkdir {mysqld_dir}; fi\"'
    os.system(cmd)

    cmd = f'ssh {sql_server_ip} ps -aef|grep mysqld|head -1'
    pid = os.popen(cmd).read().split('  ')
    print(f"mysqld pid: {pid[2]}")
    cmd = f'ssh {sql_server_ip} \"echo -n {pid[2]} > {proc_pid}\"'
    os.system(cmd)


def stop_mysql_server():
    cmd = f'ssh {sql_server_ip} \"service mariadb stop\"'
    os.system(cmd)
    cmd = f'ssh {sql_server_ip} \"rmdir {mysqld_dir}\"'
    os.system(cmd)


def setup_database(db_name):
    cmd = create_cmd + db_name;
    os.system(cmd);
    cmd = f'mysql {options} {db_name} < {create_tbl}'
    os.system(cmd)
    cmd = f'mysql {options} {db_name} < {add_fkey_idx}'
    os.system(cmd)
    cmd = f'{tpcc_load} {options} -d {db_name} -w {db_name[4:]}'
    os.system(cmd)


def drop_database(db_name):
    cmd = drop_cmd + db_name
    os.system(cmd)


def run_benchmark(db_name):
    perf = []
    cmd = f'{tpcc_run} {options} -d {db_name} -w {db_name[4:]} -P3306 \
            -c{connections} -r{warmup} -l{runtime}'
    op = os.popen(cmd).read().split('\n')
    perf.append(op[-2].split(' ')[-2])
    for line in op:
        if 'trx' in line:
            nums= line.split(',')
            if nums[0].strip(' ') == '100':
                for num in nums:
                    if '99%' in num:
                        perf.append(num.split(':')[1])
                    if '95%' in num:
                        perf.append(num.split(':')[1])
                    if 'max_rt' in num:
                        perf.append(num.split(':')[1])

    return perf


def setup_disk_quota(quota, disk):
    #setup disk quota
    cmd = f'ssh {nfs_server_ip} \"cat /sys/block/{disk}/dev\"'
    vdmajmin = os.popen(cmd).read().strip('\n')
    print(f"{disk}: {vdmajmin}")
    write = '/sys/fs/cgroup/blkio/blkio.throttle.write_bps_device'
    read = '/sys/fs/cgroup/blkio/blkio.throttle.read_bps_device'
    quota *= 1048576
    cmd = f'ssh {nfs_server_ip} \"echo -n {vdmajmin} {quota} > {write}\"'
    os.system(cmd)
    cmd = f'ssh {nfs_server_ip} \"echo -n {vdmajmin} {quota} > {read}\"'
    os.system(cmd)


def setup_cfs_period(cpu):
    #setup CPU quota
    period = f'{mysqld_dir}/cpu.cfs_period_us'
    quota = f'{mysqld_dir}/cpu.cfs_quota_us'

    cmd = f'ssh {sql_server_ip} \"echo -n 1000000 > {quota}\"'
    os.system(cmd)
    cmd = f'ssh {sql_server_ip} \"echo -n {cpu} > {period}\"'
    os.system(cmd)


# ***************************************
#*** ###easy### TPC-C Load Generator ***
#***************************************
#option h with value '192.168.122.199'
#option u with value 'root'
#option p with value 'aries123'
#option d with value 'tpcc20'
#option w with value '20'
#option P with value '3306'
#option c with value '10'
#option r with value '15'
#option l with value '180'
#<Parameters>
#     [server]: 192.168.122.199
#     [port]: 3306
#     [DBname]: tpcc20
#       [user]: root
#       [pass]: aries123
#  [warehouse]: 20
# [connection]: 10
#     [rampup]: 15 (sec.)
#    [measure]: 180 (sec.)
#
#RAMP-UP TIME.(15 sec.)
#
#MEASURING START.
#
#
#STOPPING THREADS..........
#
#<TpmC>
#                 3085.000 TpmC


def sp_start_main():
    disk = 'vdb'
    setup_nfs_server(disk)
    output_path = f'{report_base}/datacollection_tpmc.log'
    if os.path.isfile(output_path) is True:
        os.remove(output_path)

    f = open(output_path, "w+")
    f.write(f"innodb_pool_size\t wearhouse\t disk_quota\t cpu_quota\t tpmc\t mid_99%\t mid_95%\t mid_rt_max\n")
    f.flush()

    for pool in pool_size:
        setup_mysql_server(pool)
        for db_name in db_names:
            setup_database(db_name)
            for quota in disk_quota:
                setup_disk_quota(quota, disk)
                for cpu in cfs_period:
                    setup_cfs_period(cpu)
                    print(f"Running benchmark for pool:{pool} db:{db_name} disk_quota: {quota} cpu: {cpu}")
                    perf = run_benchmark(db_name)
                    print(f'TPMC: {perf[0]}')
                    line = f'{pool}\t {db_name[4:]}\t {quota}\t {cpu}\t {perf[0]}\t {perf[1]}\t {perf[2]}\t {perf[3]}\n'
                    f.write(line)
                    f.flush()
            drop_database(db_name)
        stop_mysql_server()
    f.close()


if __name__ == '__main__':
   sp_start_main()
